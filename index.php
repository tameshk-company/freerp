<?php

require_once("vendor/autoload.php");

use DB\SQL;

$f3 = Base::instance();
$f3->set('AUTOLOAD', './');
$f3->set('DEBUG', 3);




$f3->config('config.ini');

$f3->set("DB", new SQL(
    'mysql:host=localhost;port=3306;dbname=freerp',
    'root',
    '300183',
    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
));

if ((float)PCRE_VERSION < 8.0)
    trigger_error('PCRE version is out of date');

// Load configuration

$f3->route('GET /', function ($f3) {
    $f3->set('content', 'welcome.htm');
    echo \View::instance()->render('layout.htm');
});

$f3->route('GET /setup', function ($f3) {
    Models\User::setup($f3->get("DB"));
    Models\Currency::setup($f3->get("DB"));
    Models\Account::setup($f3->get("DB"));
    echo "Setup done successfully.";
});

$f3->route('POST /User/insert', "\\ViewModel\\User->insert");
$f3->route('POST /User/get', "\\ViewModel\\User->get");

$f3->route('POST /Account/insert', "\\ViewModel\\Account->insert");
$f3->route('POST /Account/get', "\\ViewModel\\Account->get");

$f3->route('POST /Currency/insert', "\\ViewModel\\Currency->insert");
$f3->route('POST /Currency/update', "\\ViewModel\\Currency->update");
$f3->route('POST /Currency/get', "\\ViewModel\\Currency->get");
$f3->route('POST /Currency/delete', "\\ViewModel\\Currency->delete");

$f3->set('ONERROR', function ($f3) {
});

$f3->run();