<?php

namespace Models;

require_once("vendor/autoload.php");

abstract class Model extends \DB\Cortex{
    protected $db = "DB";
    protected $validation;
    protected $validationErrors = [];

    function __construct()
    {
        $model = str_replace("Models\\","",get_called_class());
        $f3 = \Base::instance();
        $this->validation = \Validation::instance();
        if (!$f3->exists($model))
            $f3->config('Models/'.$model.'.ini');
        foreach ($f3->get($model) as $key => $val)
            $this->{$key} = $val;
        parent::__construct();
    }

    public function validation()
    {
        $this->validation->onError(function ($err){
            $this->validationErrors[] = $err;
        });
        $res = $this->validation->validateCortexMapper($this);
        if($res){
            $this->validationErrors = [];
            return true;
        }
        return false;
    }

    public function getValidationErrors(){
        return $this->validationErrors;
    }
}