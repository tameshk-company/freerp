<?php

namespace ViewModel {

    require_once "./ApiUtils/Response.php";

    class User
    {

        public $f3;

        public function __construct($f3)
        {
            $this->f3 = $f3;
        }

        /**
         * @param $f3 \Base
         */
        public function insert($f3)
        {
            $user = new \Models\User();
            $user->copyfrom("POST");
            $user->createdAt = date('Y-m-d H:i:s');
            if(!$user->validation()){
                header('Content-Type: application/json');
                $resp = json_encode((object)["status"=>"error","errors"=>$user->getValidationErrors()]);
                echo $resp;
                $f3->error(401);
                return;
            }
            $user->password = md5($user->password);
            try {
                $user->save();
            } catch (\PDOException $e) {
                echo json_encode($e);
            }
        }

        public function get()
        {
            $obj = new \Models\User();
            $obj->load();
            $result = [];
            while (!$obj->dry()) {
                $result[] = $obj->cast();
                $obj->next();
            }
            $response = new \Api\Response($result);
            $response->deploy();
        }

        public function setup()
        {
            \Models\User::setup();
        }
    }
}