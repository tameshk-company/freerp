<?php

namespace ViewModel;

require_once "./ApiUtils/Response.php";

class Account
{

    public $f3;

    public function __construct($f3)
    {
        $this->f3 = $f3;
    }

    public function insert()
    {
        $user = new \Models\Account();
        $user->copyfrom("POST");
        $user->createdAt = date('Y-m-d H:i:s');
        if (!$user->validation()) {
            echo "err";
            echo json_encode($user->getValidationErrors());
            return;
        }
        try {
            $user->save();
        } catch (\PDOException $e) {
            echo json_encode($e);
        }
    }

    public function get()
    {
        $obj = new \Models\Account();
        json_encode($obj->load(["_id=?", 2]));
        while (!$obj->dry()) {
            echo json_encode($obj->cast());
            $obj->next();
        }
    }

    public function setup()
    {
        \Models\Account::setup();
    }
}
