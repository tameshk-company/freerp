<?php

namespace ViewModel;

use Api\Status;
use Base;
use Cassandra\Date;

require_once "./ApiUtils/Response.php";

class Currency
{

    public $f3;

    public function __construct($f3)
    {
        $this->f3 = $f3;
    }

    /**
     * @param $f3 Base
     */
    public function insert($f3)
    {
        $currency = new \Models\Currency();
        $currency->copyfrom("POST");
        $currency->createdAt = date('Y-m-d H:i:s');
        if (!$currency->validation()) {
            $error = $currency->getValidationErrors();
            (new \Api\Response($currency->cast(), Status::WARNING, $error[0], null, 400))->deploy();
            return;
        }
        try {
            $currency->save();
            (new \Api\Response($currency->cast()["_id"]))->deploy();
        } catch (\PDOException $e) {
            (new \Api\Response(null, Status::ERROR, "Internal Server Error"))->deploy();
        }
    }

    /**
     * @param $f3 Base
     */
    public function get($f3)
    {
        $obj = new \Models\Currency();
        try {
            $obj->load(["deletedAt = ?", null]);
            $result = [];
            while (!$obj->dry()) {
                $result[] = $obj->cast();
                $obj->next();
            }
            $response = new \Api\Response([sizeof($result), $result]);
            $response->deploy();
        } catch (\PDOException $e) {
            (new \Api\Response($e, Status::ERROR, "Internal Server Error"))->deploy();
        }
    }

    /**
     * @param $f3 Base
     */
    public function delete($f3)
    {
        $currency = new \Models\Currency();
        $currency->reset();
        $currency->load(['_id = ?', (int)$f3->get("POST")["_id"]]);
        $currency["deletedAt"] = date("Y-m-d H:i:s");
        //$currency["deletedBy"] ; //TODO
        try {
            $currency->save();
            (new \Api\Response($currency->cast()["_id"]))->deploy();
        } catch (\PDOException $e) {
            (new \Api\Response($e, Status::ERROR, "Internal Server Error"))->deploy();
        }
    }

    /**
     * @param $f3 Base
     */
    public function update($f3)
    {
        $currency = new \Models\Currency();
        if ($f3->get("POST")["_id"] == "null" || $f3->get("POST")["_id"] == null) {
            (new \Api\Response(null, Status::ERROR, "Id not specified."))->deploy();
            return;
        }
        $currency->reset();
        $currency->load(["_id = ?", (int)$f3->get("POST")["_id"]]); //TODO remove it if you can
        $currency->copyfrom("POST");
        if ($currency->rate == 'null') {
            $currency->set("rate", null);
        }
        if (!$currency->validation()) {
            $error = $currency->getValidationErrors();
            (new \Api\Response($currency->cast(), Status::WARNING, $error[0], null, 400))->deploy();
            return;
        }
        try {
            $currency->save();
            (new \Api\Response())->deploy();
        } catch (\PDOException $e) {
            (new \Api\Response($e, Status::ERROR, "Internal Server Error1"))->deploy();
        }
    }

    public function setup()
    {
        \Models\Currency::setup();
    }
}
