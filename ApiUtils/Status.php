<?php
namespace Api;

require_once "Enum.php";

class Status extends \MyCLabs\Enum\Enum
{
    const SUCCESS = 'success';
    const WARNING = 'warning';
    const ERROR = 'error';
}
