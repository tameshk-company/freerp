<?php
namespace Api;

require_once "Status.php";

class Response
{

    public function __construct($data = null, $status = Status::SUCCESS, $message = '', $error = null, $response_code = null)
    {
        $this->data = $data;
        $this->status = $status;
        $this->message = $message;
        if ($error === null) {
            unset($this->error);
        } else {
            $this->error = $error;
        }
        if ($status == Status::ERROR && $response_code == null) {
            $this->response_code = 500;
        } elseif($response_code == null) {
            $this->response_code = 200;
        }else{
            $this->response_code = $response_code;
        }
    }

    public function deploy(){
        header('Content-Type: application/json');
        http_response_code($this->response_code);
        echo json_encode($this);
        file_put_contents('./log_'.date("j.n.Y").'.log', json_encode($this), FILE_APPEND);
        file_put_contents('./log_'.date("j.n.Y").'.log', " responseCode: " . $this->response_code, FILE_APPEND);
        exit();
    }


    /**
     * @var string
     */
    public $message;

    /**
     * @var Status
     */
    public $status;

    /**
     * @var string
     */
    public $error;

    /**
     * @var object
     */
    public $data;

    /**
     * @var integer
     */
    private $response_code;
}
